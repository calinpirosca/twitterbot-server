const users = require('./users/users.service.js');
const keywords = require('./keywords/keywords.service.js');
const countries = require('./countries/countries.service.js');
const followersNumber = require('./followers_number/followers_number.service.js');
const followedBy = require('./followed_by/followed_by.service.js');
const friends = require('./friends/friends.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(keywords);
  app.configure(countries);
  app.configure(followersNumber);
  app.configure(followedBy);
  app.configure(friends);
};
