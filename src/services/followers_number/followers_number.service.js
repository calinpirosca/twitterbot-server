// Initializes the `followers_number` service on path `/followers-number`
const createService = require('feathers-sequelize');
const createModel = require('../../models/followers_number.model');
const hooks = require('./followers_number.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/followers-number', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('followers-number');

  service.hooks(hooks);
};
