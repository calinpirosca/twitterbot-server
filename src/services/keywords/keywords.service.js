// Initializes the `keywords` service on path `/keywords`
const createService = require('feathers-sequelize');
const createModel = require('../../models/keywords.model');
const hooks = require('./keywords.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/keywords', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('keywords');

  service.hooks(hooks);
};
