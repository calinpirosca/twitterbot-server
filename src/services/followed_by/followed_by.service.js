// Initializes the `followed_by` service on path `/followed-by`
const createService = require('feathers-sequelize');
const createModel = require('../../models/followed_by.model');
const hooks = require('./followed_by.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/followed-by', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('followed-by');

  service.hooks(hooks);
};
