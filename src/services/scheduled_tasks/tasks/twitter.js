const cron = require('node-cron');
const app = require('./../../../app');
const config = require('./config.js');
const Twitter = require('twitter');
const T = new Twitter(config);

const twitterAPI = () => {
  checkFollowers();
  searchNewFriend();
};

/**
 * Check if the people that were followed followed back within 30 days
 */
const checkFollowers = () => {
  const friendsService = app.service('friends');
  console.log('checkFollowers');
  //TODO
};

/**
 * Search for a new friend to follow
 */
const searchNewFriend = async () => {
  const toFollowToday = Math.floor(Math.random() * 51);

  const {keywords, countries, followedBy, followersNo} = await getQuery();
  //TODO follow people during random hours of day rather than at midnight

  let UserParams = {
    q: keywords.join(),
    count: 1,
    page: 1,
    include_entities: false
  };

  let user;
  let followed = false;
  for(let i = 0;i<toFollowToday;i++){
    do {
      user = await T.get('users/search', UserParams);
      if(user.length !== 0) { // Any user matching the given query ?
        console.log(user[0].screen_name + ' ' + user[0].id_str + ' ' + user[0].followers_count);
        followed = user[0].following;
        if(followed) { // Already following the user ?
          UserParams.page = ++UserParams.page;
        } else {
          try{
            followed = await validatePossibleFriend(user[0], {countries, followedBy, followersNo});
            if(followed) {
              await T.get('friendships/create', {screen_name: user[0].screen_name});
              await app.service('friends').post({screen_name: user[0].screen_name});
              console.log('Followed ' + user[0].screen_name);
            } else {
              followed = true;
              UserParams.page = ++UserParams.page;
            }
          }catch(err){
            console.log(err);
            //Todo rate limit exceeded perhaps, stop search
          }
        }
      }
    } while(user.length !== 0 && followed === true);
  }
};

/**
 * Follow the new user if it fits the searching criteria
 * @param user
 * @param criteria
 */
const validatePossibleFriend = async (user, criteria) => {
  criteria.countries.push('United States');
  const initialCondition = Math.min(criteria.followersNo <= user.followers_count) &&
    criteria.countries.includes(user.location);
  if(initialCondition) {   // Does it have the minimum number of followers && location ?
    // Does it have the followed_by property ?
    let found = false;
    for(let follower of criteria.followedBy) {
      let followers;
      let followersParams = {
        screen_name: user.screen_name,
        cursor: -1
      };
      while(!found) {
        try {
          followers = await T.get('followers/list', followersParams);
          followers.users.filter(agent => agent.screen_name === follower.screen_name).length > 0 ?
            found = true :
            followersParams.cursor = followers.next_cursor_str;
          if(followersParams.cursor === 0) {
            break;
          }
        }catch(err){
          throw err;
        }
      }
    }
    return found;
  } else {
    return false;
  }
};

const getQuery = async () => {
  const keywordsService = app.service('keywords');
  const countriesService = app.service('countries');
  const followedByService = app.service('followed-by');
  const followersNumberService = app.service('followers-number');

  const keywords = await keywordsService.find();
  const countries = await countriesService.find();
  const followedBy = await followedByService.find();
  const followersNo = await followersNumberService.find();

  return {
    keywords: keywords.data.map(a => a.keyword),
    countries: countries.data.map(a => a.location),
    followedBy: followedBy.data.map(a => a.screenName),
    followersNo: followersNo.data.map(a => a.number)
  };
};

module.exports = {
  twitterAPI
};
