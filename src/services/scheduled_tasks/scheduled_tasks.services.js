const cron = require('node-cron');
const TwitterAPI = require('./tasks/twitter').twitterAPI;

/**
 * Initialize cron jobs
 */
const initializeScheduledTasks = () => {
  cron.schedule('0 0 * * *', () => {
    TwitterAPI();
  });
};

module.exports = {
  initializeScheduledTasks
};

